package main

import (
	"gitlab.com/MikeTTh/env"
	"gitlab.com/pal.toth/balint-rethelyi-platform-engineer/web"
	"log"
)

func main() {
	e := web.Router.Run(env.String("PORT", ":8000"))
	if e != nil {
		log.Fatal(e)
	}
}
