FROM alpine:latest

WORKDIR /app

ADD app /app/app

EXPOSE 8000

ENTRYPOINT ["/app/app"]