# balint rethelyi platform engineer

## Description
**Containerinfo** is a basic Golang application, that gets information about every
container of a Kubernetes cluster.

## Installation
To deploy in your cluster, use the provided Helm Chart in the `helm` directory.
First edit `values.yaml`.

You have to specify a role and a ServiceAccount, because Containerinfo needs
readonly permissions, so it can retrieve the pods, containers and their 
resource requests and limits (if they exist).

If you want to access Containerinfo out of your cluster, you should create
an Ingress. For that, specify your Ingress class, and if you want to use https,
your issuer too, and of course the expected domain name as well.

```bash
helm install containerinfo .
```

Uninstall:

```bash
helm uninstall containerinfo
```

Upgrade (or install if needed):

```bash
helm upgrade --install containerinfo .
```

## Usage
If you want to try it inside your cluster, you need to exec in a pod and run `curl`
(if you need a `debug` pod, check out `test.yml` in `helm` directory).

Inside the cluster:

```bash
curl "http://<service name>.<namespace>.svc.cluster.local/container-resorces?pod-label=<label>"
```

For example:
```bash
curl "http://containerinfo-service.prezi-test.svc.cluster.local/container-resorces?pod-label=app.kubernetes.io%2Fcomponent%3Dcontroller"
```

Out of the cluster:

You have to `curl` the IP or URL of your Ingress.

For example:
```bash
curl "https://containerinfo.rethelyi.hu/container-resorces?pod-label=app.kubernetes.io%2Fcomponent%3Dcontroller"
```

