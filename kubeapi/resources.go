package kubeapi

import (
	"errors"
	"reflect"
)

type Container struct {
	ContainerName string
	PodName       string
	Namespace     string
	MemReq        *string
	MemLimit      *string
	CpuReq        *string
	CpuLimit      *string
}

//strPtr is a  helper function
//returns the pointer of the string
//received as input
func strPtr(str string) *string {
	return &str
}

// ifNotExists is a helper function, it returns
// nil, if the object exists, or a custom error
// if not exists
func ifNotExists(obj interface{}) error {
	v := reflect.ValueOf(obj)
	if v.IsNil() {
		return errors.New("notExists")
	}
	return nil
}

// GetContainerResourcesByLabel get container(s) resources by a Kubernetes label
func GetContainerResourcesByLabel(podLabel string) ([]*Container, error) {
	pods, err := getPods(podLabel)
	if err != nil {
		return nil, err
	}
	var cnts []*Container

	for _, pod := range pods.Items {

		containers := pod.Spec.Containers

		for _, container := range containers {
			cnt := Container{
				ContainerName: container.Name,
				PodName:       pod.Name,
				Namespace:     pod.Namespace,
			}

			// check if the container has existing resource requests
			err := ifNotExists(container.Resources.Requests)
			if err != nil {
				cnt.CpuReq = nil
				cnt.MemReq = nil
			} else {
				// if exists, get them
				cnt.CpuReq = strPtr(container.Resources.Requests.Cpu().String())
				cnt.MemReq = strPtr(container.Resources.Requests.Memory().String())
			}

			// check if the container has existing resource limimts
			err = ifNotExists(container.Resources.Limits)
			if err != nil {
				cnt.CpuLimit = nil
				cnt.MemLimit = nil
			} else {
				// if exists, get them
				cnt.CpuLimit = strPtr(container.Resources.Limits.Cpu().String())
				cnt.MemLimit = strPtr(container.Resources.Limits.Memory().String())
			}

			cnts = append(cnts, &cnt)
		}

	}

	return cnts, nil
}
