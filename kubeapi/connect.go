package kubeapi

import (
	"context"
	"flag"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	_ "k8s.io/client-go/tools/portforward"
	_ "k8s.io/client-go/transport/spdy"
	"k8s.io/client-go/util/homedir"
	"path/filepath"
)

var kubeconfig *string
var clientset *kubernetes.Clientset

// init initialize the cluster config
// and connect to the cluster
func init() {
	// out-of-cluster config
	if home := homedir.HomeDir(); home != "" {
		kubeconfig = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfig = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.Parse()

	// try to connect from inside the cluster to the cluster
	config, err := rest.InClusterConfig()
	if err != nil {
		config, err = outOfClusterConnect()
		if err != nil {
			panic(err)
		}
	}
	// creates the clientset
	clientset, err = kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}
}

// outOfClusterConnect try to connect
// from out of the cluster to the cluster
func outOfClusterConnect() (*rest.Config, error) {
	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	if err != nil {
		return nil, err
	}
	return config, nil
}

// getPods returns all pods from cluster
func getPods(podLabel string) (*v1.PodList, error) {

	pods, err := clientset.CoreV1().Pods("").List(context.TODO(), metav1.ListOptions{
		LabelSelector: podLabel,
	})
	if err != nil {
		return nil, err
	}

	return pods, nil
}
