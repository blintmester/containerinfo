package web

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pal.toth/balint-rethelyi-platform-engineer/kubeapi"
	"net/http"
)

var Router = gin.Default()

func init() {
	Router.Use(errorHandler)
	Router.GET("/container-resorces", resourcesHandler)
}

func errorHandler(c *gin.Context) {
	c.Next()
	if len(c.Errors) != 0 {
		code := http.StatusInternalServerError
		c.JSON(code, c.Errors)
	}
}

func resourcesHandler(c *gin.Context) {
	podLabel := c.Query("pod-label")
	cnts, err := kubeapi.GetContainerResourcesByLabel(podLabel)
	if err != nil {
		_ = c.Error(err)
		return
	}
	c.JSON(http.StatusOK, cnts)
}
